
require 'rubygems'
require 'sinatra/base'
require 'sinatra/assetpack'
require 'haml'
require 'sass'
require 'bourbon'
require 'sinatra/partial'

class Voiping < Sinatra::Base

  set :bind, '0.0.0.0'
  set :root, File.dirname(__FILE__)
  set :views, settings.root + '/views' 
  set :haml, :format => :html5
  set :sass, { :load_paths => [File.dirname(__FILE__) + "/css/sartoriadelweb", File.dirname(__FILE__) + "/css/sartoriadelweb/library", File.dirname(__FILE__) + "/css/sartoriadelweb/layouts", File.dirname(__FILE__) + "/css/sartoriadelweb/modules", File.dirname(__FILE__) + "/css/sartoriadelweb/scaffold" ] }
  
  enable :inline_templates

  register Sinatra::AssetPack
  register Sinatra::Partial

  assets do

    serve '/js',     from: 'js'
    serve '/css',    from: 'css'
    serve '/img',    from: 'img'
    serve '/fonts',  from: 'fonts/sartoriadelweb'


    css :bundle, [
      '/css/sartoriadelweb/scaffold/bundle.css'
    ]

    css :library, [
      '/css/sartoriadelweb/library/library.css',
      '/css/jquery.bxslider.css',
      '/css/jquery.nivo.slider.css',
      '/css/jquery.nivo.theme.light.css'
    ]

    css :marketing, [
      '/css/sartoriadelweb/marketing.css'
    ]       

    js :marketing, [
     '/js/sartoriadelweb/marketing.js'
    ]       

    js :bundle, [
     '/js/sartoriadelweb/bundle.js'
    ]

    js :library, [
     '/js/vendor/jquery.*.js'
    ]       

    #expires 86400*0, :public
    #cache_dynamic_assets true

    css_compression :sass
    js_compression :uglify, :mangle => false

  end

  get '/' do
    haml :index, :views => settings.root + '/views/marketing'
  end  

  get '/about' do
    haml :about, :views => settings.root + '/views/marketing/content', :layout => :'../layout'
  end 

  get '/services' do
    haml :services, :views => settings.root + '/views/marketing/content', :layout => :'../layout'
  end 

  get '/network' do
    haml :network, :views => settings.root + '/views/marketing/content', :layout => :'../layout'
  end 

  get '/contact' do
    haml :contact, :views => settings.root + '/views/marketing/content', :layout => :'../layout'
  end       

run! if app_file == $0
end

